# Image analysis examples for plant scientists

:warning: **material under development** :construction:


#### Ideas

- Problem-focused modules to teach image analysis methods focusing on plant data
- Each module consists of a dataset and a question the researcher wanted to answer
- Use real data provided by researchers
- As the solution to each problem is taught, individual concepts can be discussed (e.g. thresholding, morphological operations, convolutions, etc...)


#### Contributing to the materials

- Each module has its own directory (e.g. [`circadian-seedlings`](circadian-seedlings))
- Within it there's a zip-compressed `data.zip` file with the images used (e.g. [`circadian-seedlings/data.zip`](circadian-seedlings/data.zip))
  - we could use uncompressed `data/` directory, but it may blow up the repository size quite a bit...
- A markdown file is used for the lesson materials (e.g. [`circadian-seedlings.md`](circadian-seedlings/circadian-seedlings.md))
- At this early stage of development each contributor can push their own solution script by prefixing the script with their name initials (e.g. [`ht-seedling_segmentation.py`](circadian-seedlings/ht-seedling_segmentation.py))
  - at the top of the script have a multi-line string (triple quoted `"""`) summarising what the script is doing and any limitations.
  - comment code extensively.


#### Other resources

- concepts-based materials: https://grp-bio-it.embl-community.io/image-analysis-training-resources/
- a python image-analysis course:  https://git.embl.de/grp-bio-it/image-analysis-with-python
- A couple of papers worth reading: https://dev.biologists.org/content/139/17/3071 ; https://dx.doi.org/10.1371%2Fjournal.pcbi.1000603
- blog post with glossary of terms: https://usabilityetc.com/articles/image-processing-glossary/


#### Tools

- [python/scikit-image](https://scikit-image.org/) - very comprehensive package
- [R/EBImage](https://www.bioconductor.org/packages/release/bioc/vignettes/EBImage/inst/doc/EBImage-introduction.html) - decent but not as comprehensive as python
- [Fiji](https://fiji.sc/) - comprehensive GUI software (also has its own scripting language)

`python/skimage` seems the best choice, although it comes with the overhead of the syntax.
Fiji is a good alternative as many biologists are familiar with it.
