# Analysis of circadian clock gene expression in Arabidopsis seedlings

## Summary

- Type of imaging: bioluminescent imaging
- Tissue: Arabidopsis seedlings
- Data files: tif files named `LD-to-LL00[00-94].tif`
- Source: kindly provided by Mark Greenwood and James Locke ([Greenwood et al 2019](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.3000407))

## Data description

The data provided is a time-lapse of a single seedling imaged throughout XX hours.

![](seedling_example.png)

- Seedlings were grown for 4d under long day cycles and then imaged under constant light.
- Luciferase imaging for a circadian marker: GI::LUC (*Gigantia* gene promoter with luciferase reporter).

## Objective

Quantify the gene's expression along the apical-basal axis of the seedling (from root to cotyledons).

The results should provide with data to recreate something similar to
[Fig. 2H-I](https://journals.plos.org/plosbiology/article/figure/image?size=medium&id=info:doi/10.1371/journal.pbio.3000407.g002)
of the publication.

## Analysis pipeline

We can break this problem into the following steps:

1. Segment the seedling from the background
2. Create a "skeleton" of the root and hypocotyl
3. Quantify the intensity along that "skeleton"
4. Repeat 1-3 for each time point (image)

These steps are illustrated here:

![](./seedling_pipeline.png)

The code is:

```python
def process_seedling(image):
    image_processed = exposure.equalize_hist(image)
    image_processed = filters.median(image_processed)
    seedling_mask = image_processed > np.quantile(image_processed, .75)
    seedling_mask = morphology.opening(seedling_mask , morphology.square(2))
    seedling_skeleton = morphology.skeletonize(seedling_mask)
    return(seedling_skeleton)
```

This returns the skeleton, which can be multiplied by the original image to return the pixel intensity on the skeleton.

One can then obtain the following:

![](./seedling_kymograph.png)

Which is roughly representative of the data.


#### 1. Segmentation - defining mask

Segmentation is the process of detecting a region of interest.

- Pre-processing:
  - histogram equalisation for improved contrast
- Create segmentation mask:
  - Mean thresholding - worked alright (was used in the paper)
- Canny edge detector + filling?

#### 2.

- From the mask an opening operation (erosion > dilation) might get rid of the root + hypocotyl because they're thinner than the cotiledons.


#### 3. Skeletonise

- `skeletonize` function in skimage in the mask


#### 4.

- Just take the intensity of the skeleton's pixel
- Or an average of a small structuring element around the pixel



Cover any fundamental concepts that arise. For example (this is invented):

- Preprocessing
  - filter operation: *2D Gaussian blurring* to remove some background noise
  - exposure operation: *equalise* histogram to increase contrast between seedling and background
- Segmentation to create mask
  - filter operation: *threshold* to binarise the image
  - morphological operation: *opening* to remove small patches of signal outside the seedling
  - morphological operation: *closing* to make the signal inside the seedling homogeneous
- Quantification:
  - multiply mask by original image to eliminate background signal
  - for each row of the image matrix calculate average pixel intensity (or any other metrics)
- Apply to all images:
  - wrap the above pipeline in a function
  - apply the function to all images and export results as CSV file

## Data analysis

Present a final analysis, usually as a graph.
