#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 14:18:45 2019

@author: henrik
"""
import numpy as np
from os import listdir
from skimage.io import imread
from skimage.exposure import equalize_adapthist
from skimage.exposure import equalize_hist
from skimage.morphology import binary_closing
from skimage.morphology import binary_opening
from skimage.morphology import skeletonize
from skimage.morphology import label
from skimage.measure import regionprops

files = listdir('./data/')

ll = []
for img_path in files:
    img = imread('./data/' + img_path)
    img = equalize_adapthist(img) # preprocess
    img = (img.T > np.quantile(img, .9, axis=1)).T # filter out relevant parts
    img = binary_closing(img)
    img = binary_opening(img)
    labels = label(img)

    # Get only the largest blob
    props = regionprops(labels)
    areas = [pp.area for pp in props]
    biggest = np.argmax(areas)
    labels[labels != props[biggest].label] = 0
    ll.append(labels)

# Remove some noise that is only included in a few roots
stack = np.dstack(ll).T
noise = np.sum(stack > 0, axis=0) < 10
data = []
for ii in range(stack.shape[0]):
    stack[ii, noise] = 0
    img = tiff.imread(files[ii]).T
    img = img[skeletonize(stack[ii] > 0)]
    data.append(np.mean(img))

plot(data)
