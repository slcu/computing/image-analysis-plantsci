#!/bin/python

"""
Analysis of seedling luciferase reporter.
This is coded as an interactive script (run line-by-line).

- equalised histogram to improve contrast (but maybe this could be improved...)

- Manually thresholded the image - ideally this should be automated
(quantile of the histogram? Sample points in the perifery to define background?)

- opening operation removed occasional bright pixels outside the seedling

- multiplied the mask by the original image and then iterated through rows to calculate average intensity
"""

#### Imports ####

from skimage.io import imread
from skimage import exposure
from skimage import morphology
from skimage import filters

import numpy as np

import matplotlib.pyplot as plt

from os import listdir


#### Read data ####

# read data
image = imread("data/LD-to-LL0000.tif")
type(image) # returns numpy ndarray
image.shape # dimensions of array
image.min()
image.max()

# scale values to the maximum (so we have range 0-1)
#image = (image - image.min())/image.max()

#### Improve contrast ####

# This is the original image
plt.subplot(231)
plt.imshow(image)
plt.subplot(234)
plt.hist(image.flatten(), bins = 50)

# equalise pixel intensity
plt.subplot(232)
plt.imshow(exposure.equalize_adapthist(image))  # OK, improves contrast a little
plt.subplot(235)
plt.hist(exposure.equalize_adapthist(image).flatten(), bins = 50)

plt.subplot(233)
plt.imshow(exposure.equalize_hist(image))  # too much
plt.subplot(236)
plt.hist(exposure.equalize_hist(image).flatten(), bins = 50)

image_equalised = exposure.equalize_hist(image)

# median filter to remove some background noise
image_processed = filters.median(image_equalised)


#### create whole seedling mask ####

# Doing a mean thresholding works well
seedling_mask = image_processed > np.quantile(image_processed, .75)

plt.subplot(141)
plt.imshow(image)
plt.subplot(142)
plt.imshow(image_processed)
plt.subplot(143)
plt.imshow(seedling_mask )

# Opening operation (Erosion -> Dilation) to remove small pixels
plt.subplot(144)
plt.imshow(morphology.opening(seedling_mask , morphology.square(2)))

seedling_mask = morphology.opening(seedling_mask , morphology.square(2))

# now the mask can be applied to the original image
# there's still some "aura" around the seedling that looks like background...
plt.imshow(image * seedling_mask )

seedling_segmented = image * seedling_mask 

# extract average pixel intensity across rows
# should probably only calculate average on those pixels that have a signal
plt.plot(np.mean(seedling_segmented, axis = 1))


#### skeletonize seedling ####

# there's also morphology.medial_axis
seedling_skeleton = morphology.skeletonize(seedling_mask)

plt.subplot(131)
plt.imshow(seedling_mask)
plt.subplot(132)
plt.imshow(seedling_skeleton)
plt.subplot(133)
plt.imshow(image * seedling_skeleton)

# get value along y-axis
np.max(image*seedling_skeleton, axis = 1)


# represent the pipeline in a graph
plt.subplot(231)
plt.imshow(image)
plt.title("Original")
plt.subplot(232)
plt.imshow(image_equalised)
plt.title("Equalised")
plt.subplot(233)
plt.imshow(image_processed)
plt.title("Median Filter")
plt.subplot(234)
plt.imshow(seedling_mask)
plt.title("Quantile threshold")
plt.subplot(235)
plt.imshow(seedling_skeleton)
plt.title("Skeletonised")
plt.subplot(236)
plt.imshow(image)
plt.imshow(1 - seedling_skeleton, alpha = 0.5, cmap = "Greys")
plt.title("Overlap")


#### apply accross all images ####

# define a function that does all these steps
def process_seedling(image):
    image_processed = exposure.equalize_hist(image)
    image_processed = filters.median(image_processed)
    seedling_mask = image_processed > np.quantile(image_processed, .75)
    seedling_mask = morphology.opening(seedling_mask , morphology.square(2))
    seedling_skeleton = morphology.skeletonize(seedling_mask)
    #return(np.max(image*seedling_skeleton, axis = 1))
    return(seedling_skeleton)

# get list of files 
image_files = sorted(listdir("./data"))

images = []
images_skeletons = []

results = np.array([0]*134)

for i in range(len(image_files)):
    images.append(imread("./data/" + image_files[i]))
    i_skeleton = process_seedling(images[i])
    images_skeletons.append(i_skeleton)
    i_result = np.max(images[i]*images_skeletons[i], axis = 1)
    results = np.column_stack((results, i_result))
    

# display data normalised by the maximum in each column (= seedling skeleton)
plt.imshow(results/np.max(results, axis = 0))
plt.colorbar()

fig = plt.figure(figsize=(50, 50))
for i in range(30):
    offset = 50
    sub = fig.add_subplot(3, 10, i + 1)
    sub.imshow(filters.median(exposure.equalize_hist(images[i+offset])))
    sub.imshow(images_skeletons[i+offset], alpha = 0.5)
    sub.set_title(image_files[i+offset])
