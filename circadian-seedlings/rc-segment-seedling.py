from skimage.io import imread
from skimage import exposure
from skimage import morphology
from skimage import filters
from skimage.segmentation import active_contour, chan_vese
from skimage import feature


import numpy as np
import matplotlib.pyplot as plt

image = imread("data/LD-to-LL0000.tif")

print(image.shape)

plt.subplot(131)
plt.imshow(image)
plt.subplot(132)
plt.imshow(exposure.equalize_adapthist(image))  # OK, improves contrast a little
plt.subplot(133)
plt.imshow(exposure.equalize_hist(image))
plt.show()

image_processed = exposure.equalize_adapthist(image)

plt.subplot(121)
plt.imshow(image_processed)
plt.subplot(122)
plt.imshow(feature.canny(image_processed))
plt.show()

from skimage import data
from skimage import color
from skimage.filters import meijering, sato, frangi, hessian
import matplotlib.pyplot as plt


def identity(image, **kwargs):
    """Return the original image, ignoring any kwargs."""
    return image

image = image_processed

cmap = plt.cm.gray

kwargs = {}
kwargs['sigmas'] = [1]

fig, axes = plt.subplots(2, 5)
for i, black_ridges in enumerate([1, 0]):
    for j, func in enumerate([identity, meijering, sato, frangi, hessian]):
        kwargs['black_ridges'] = black_ridges
        result = func(image, **kwargs)
        if func in (meijering, frangi):
            # Crop by 4 pixels for rendering purpose.
            result = result[4:-4, 4:-4]
        axes[i, j].imshow(result, cmap=cmap, aspect='auto')
        if i == 0:
            axes[i, j].set_title(['Original\nimage', 'Meijering\nneuriteness',
                                  'Sato\ntubeness', 'Frangi\nvesselness',
                                  'Hessian\nvesselness'][j])
        if j == 0:
            axes[i, j].set_ylabel('black_ridges = ' + str(bool(black_ridges)))
        axes[i, j].set_xticks([])
        axes[i, j].set_yticks([])

plt.tight_layout()
plt.show()

from skimage.morphology import skeletonize
skeleton = skeletonize(image>np.mean(image))

plt.subplot(121)
plt.imshow(image_processed)
plt.subplot(122)
plt.imshow(image)
plt.imshow(skeleton, alpha=0.5)
plt.show()


