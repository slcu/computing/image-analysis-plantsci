

### General python notes

- Mention about plotting with matplotlib:
  - `plt.subplot(131)` to create plot panels (this means 1 row, 3 columns and this is the first of the plots)

## imaging processing concepts

### Task: segmentation

#### thresholding

Can be used for segmentation. Different methods to determine threshold.

- if ROI occupies a lot of the image, then mean thresholding might not work so well (because the mean will include a lot of the pixels of interest)
- if on the other hand there's a lot of background, mean might work well, because the mean will be somewhere between the value of your ROI and the background
-

#### Active contour

A bit harder to parametrise. We could not really figure it out immediately.

#### 
