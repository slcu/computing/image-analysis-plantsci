#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 10:48:00 2019

@author: henrik
"""

import tifffile as tiff
import cv2
import numpy as np
#from clahe import clahe
import mahotas as mh

img = tiff.imread('./data/NAA_A2.tif')[0]
# img = clahe(img,
#            np.round(np.array(img.shape) / 8).astype(int),
#            clip_limit=1) # contrast adjustment?
img = (img - img.min()) / (img.max() - img.min()) * 255 # rescale
img = img.astype('uint8')
img[img < mh.otsu(img)] = 0 # remove some background

# Convert to grayscale
img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Blur
kernel_size = 5
blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)

# Edge detect
low_threshold = 75
high_threshold = 150
edges = cv2.Canny(blur_gray, low_threshold, high_threshold)

# HoughPLines -- These arev the parameters that may need tweaking
rho = 1  # distance resolution in pixels of the Hough grid
theta = np.pi / 180  # angular resolution in radians of the Hough grid
threshold = 15  # minimum number of votes (intersections in Hough grid cell)
min_line_length = 20  # minimum number of pixels making up a line
max_line_gap = 5  # maximum gap in pixels between connectable line segments
line_image = np.copy(img) * 0  # creating a blank to draw lines on

# Run Hough on edge detected image
# Output "lines" is an array containing endpoints of detected line segments
lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]),
                        min_line_length, max_line_gap)

for line in lines:
    for x1, y1, x2, y2 in line:
        cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 5)

# Draw the lines on the  image
lines_edges = cv2.addWeighted(img, 0.8, line_image, 1, 0)
tiff.imshow(np.hstack([gray, edges, lines_edges[..., 0]]), cmap='plasma')
