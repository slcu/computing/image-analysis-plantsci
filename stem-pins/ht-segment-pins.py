# import packages
from skimage.io import imread
from skimage import exposure
#from skimage import morphology
#from skimage import filters
#from skimage.segmentation import active_contour, chan_vese
from skimage import feature

#import numpy as np
import matplotlib.pyplot as plt


# read image
image = imread("./data/NAA_A2.tif")

# visualise two channels
plt.subplot(121)
plt.imshow(image[0])
plt.subplot(122)
plt.imshow(image[1])


# check histogram equalisation
plt.subplot(131)
plt.imshow(image[0])
plt.subplot(132)
plt.imshow(exposure.equalize_adapthist(image[0]))  # OK, improves contrast a little
plt.subplot(133)
plt.imshow(exposure.equalize_hist(image[0])) # too much
plt.show()

image_processed = exposure.equalize_adapthist(image[0])
plt.imshow(image_processed)

from skimage.segmentation import slic

image_slic = slic(image_processed)
plt.imshow(image_slic)

from skimage.segmentation import felzenszwalb
plt.imshow(felzenszwalb(image_processed))


### Trying a k-means on HOG descriptor ###

fd, hog_image = feature.hog(image_processed, orientations=9, pixels_per_cell=(2, 2), cells_per_block=(1, 1), transform_sqrt=True, block_norm="L1", visualize = True, feature_vector = False)


plt.subplot(121)
plt.imshow(image_processed)
plt.subplot(122)
plt.imshow(hog_image)

fd.size
fd.shape
image_processed.shape

fd[0:10, 0:10, 0, 0, 3]

plt.hist(fd)
