from skimage.io import imread
from skimage import exposure
from skimage import morphology
from skimage import filters
from skimage.feature import canny
from skimage.transform import probabilistic_hough_line

import numpy as np

import matplotlib.pyplot as plt


img = imread('./data/NAA_A3.tif')[0]
img = (img - img.min()) / float((img.max() - img.min())) * 255 # rescale
img = img.astype('uint8')

"""
f, ax = plt.subplots(ncols=5)
ax[0].imshow(img)
ax[0].set_title('original')
for imageindex, sigma in enumerate([1, 3, 5, 7]):
   img_filtered = filters.median(img, morphology.square(sigma))
   ax[imageindex+1].imshow(img_filtered)
   ax[imageindex+1].set_title('median, square size='+str(sigma))
plt.show()"""

img_contrast = exposure.equalize_adapthist(img)
img_filtered = filters.median(img, morphology.square(3))


img_threshold = img_filtered > np.quantile(img_filtered, 0.995)
img_threshold = morphology.closing(img_threshold, morphology.square(5))
img_skeleton = morphology.skeletonize(img_threshold)

f, ax = plt.subplots(ncols=3)
ax[0].imshow(img_filtered)
ax[1].imshow(img_threshold)
ax[2].imshow(img_skeleton)

lines = probabilistic_hough_line(img_skeleton, line_length=20, line_gap = 5)

f, ax = plt.subplots(ncols=3)
ax[0].imshow(img_filtered)
ax[1].imshow(img_skeleton)
ax[2].imshow(np.zeros((img_skeleton.shape[0], img_skeleton.shape[1])))
for l in lines:
   ax[2].plot((l[0][0], l[1][0]), (l[0][1], l[1][1]))
plt.show()