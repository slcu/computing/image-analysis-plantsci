# Analysis of polar PIN protein in Arabidopsis stem segments

## Summary

- Type of imaging: PIN1-GFP
- Tissue: Arabidopsis stem
- Data files: tif files
- Source: kindly provided by Anthony Bridgen (unpublished)

## Problem

The data are stems (xylem parenchyma) of an Arabidopsis PIN1-GFP line:

![](pin_example.png)

The green channel is GFP and the red channel is chlorophyll autofluorescence.

The objective is to quantify the intensity of polarly-localised PIN.
These appear as "stripes" of GFP signal.

The intensity of the polarly-localised PIN needs to somehow be normalised to the background.


**bonus**: extract a measure of "polarity" for each pin blob

## Analysis pipeline

...
